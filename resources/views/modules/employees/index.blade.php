@extends('layouts.app')

@section('content')

<div class="content p-3">
    <div class="card">
        <div class="card-body">
            <h3>List of Employees</h3>
            <br>
            <button type="button" class="btn btn-sm mb-3 text-white" style="background-color: #0f5570 !important;"
                data-toggle="modal" data-target="#create">
                <i class="fa fa-plus"></i> &nbsp; | &nbsp; Create new company
            </button>
           <div class="table-responsive">
            <table class="table table-striped" id="employeetable">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Full Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">Phone</th>
                        <th scope="col">Company</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
            </table>
           </div>

            {{-- Modal --}}
            @include('modules.employees.modal.create')
            @include('modules.employees.modal.update')
            @include('modules.employees.modal.company')
        </div>
    </div>
</div>
@endsection
@section('js')
<script>
    $(document).ready(function(){
        $('#employeetable').DataTable({
            processing:true,
            serverSide:true,
            ajax: {
                url: "{{route('employees.getEmployeeData')}}",

            },
            columns: [
                {
                    name:'DT_RowIndex',
                    data:'DT_RowIndex',
                    orderable: false,
                    searchable: false
                },
                {
                    name:'fullname',
                    data:'fullname'
                },
                {
                    name:'email',
                    data:'email'
                },
                {
                    name:'phone',
                    data:'phone'
                },
                {
                    name:'company',
                    data:'company'
                },
                {
                    name:'action',
                    data:'action',
                    orderable:false
                },

            ]
        });
    });
</script>
<script>
      function createEmployee (e) {
        e.preventDefault();
            const url = `/employees`;
             axios.post(url,{
                firstname : $('input[name=firstname]').val(),
                lastname : $('input[name=lastname]').val(),
                company : $('select[name=company]').val(),
                phone : $('input[name=phone]').val(),
                email : $('input[name=email]').val(),
            })
            .then(res => {
                $('#createEmployee')[0].reset();
                $("#create").modal('hide');
                iziToast.success({
                    title: 'OK',
                    message: res.data.msg,
                });
                $('#employeetable').DataTable().ajax.reload();
            })
            .catch(e => {
                  console.error(e);
                iziToast.error({
                    title: 'Error',
                    message: e.message,
                });
            });
           }

           async function companyModal(employee, company){
            try{
                const url = `/employees/findcompany/${company}`;
                const res = await axios(url);

                $(`#companyname-${employee}`).val(res.data.name);
                $(`#companyemail-${employee}`).val(res.data.email);
                $(`#companywebsite-${employee}`).val(res.data.website);
                if(res.data.logo){
                    let srcData = `{{ URL::asset('/storage/${res.data.logo}')}}`;
                    $(`#companylogo-${employee}`).attr('src', srcData)
                }else{
                    let srcData = `{{ URL::asset('/company.jpg')}}`;
                    $(`#companylogo-${employee}`).attr('src', srcData)
                }
            }
            catch(e){
                console.error(e);
            }
        }


         function updateEmployee (e, employee) {
            e.preventDefault();
            const url = `/employees/${employee}`;
             axios.put(url,{
                firstname : $(`#firstname-${employee}`).val(),
                lastname : $(`#lastname-${employee}`).val(),
                company : $(`#company-${employee}`).val(),
                phone : $(`#phone-${employee}`).val(),
                email : $(`#email-${employee}`).val(),
            })
            .then(res => {
                $(`#update-${employee}`).modal('hide');
                iziToast.success({
                    title: 'OK',
                    message: res.data.msg,
                });
                $('#employeetable').DataTable().ajax.reload();
            })
            .catch(e => {
                  console.error(e);
                iziToast.error({
                    title: 'Error',
                    message: e.message,
                });
            });
        }

     function deleteEmployee (employee) {
        if(confirm("Are you sure?")){
            const url = `/employees/${employee}`;
             axios.delete(url)
            .then(res => {
                iziToast.success({
                    title: 'OK',
                    message: res.data.msg,
                });
                $('#employeetable').DataTable().ajax.reload();
            })
            .catch(e => {
                  console.error(e);
                iziToast.error({
                    title: 'Error',
                    message: e.message,
                });
            });
        }
    }
</script>
@endsection
