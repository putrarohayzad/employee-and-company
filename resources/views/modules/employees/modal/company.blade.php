@foreach ($employees as $employee)
<div class="modal fade" id="companyemployee-{{$employee->id}}" tabindex="-1" role="dialog" aria-labelledby="updatemodal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="updatemodal">{{optional($employee->getCompany)->name}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form>
                <div class="modal-body">
                    <div role="group" class="form-group text-center">
                        @if (optional($employee->getCompany)->logo)
                        <img id="companylogo-{{$employee->id}}" alt="logo"
                            style="width: 100px; height:100px">
                        @else
                        <img src="{{asset('company.jpg')}}" alt="logo" width="250" height="250">
                        @endif
                    </div>
                    <div role="group" class="form-group">
                        <div class="form-row">
                            <label class="form-label">Name</label>
                            <input type="text" class="form-control" id="companyname-{{$employee->id}}" name="name2" readonly>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div role="group" class="form-group">
                                <div class="form-row">
                                    <label class="form-label">Email</label>
                                    <input type="email" class="form-control" id="companyemail-{{$employee->id}}" name="email2" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div role="group" class="form-group">
                                <div class="form-row">
                                    <label class="form-label">Website</label>
                                    <input type="text" class="form-control" id="companywebsite-{{$employee->id}}" name="website2"  readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                 

                </div>
            </form>
        </div>
    </div>
</div>
@endforeach
