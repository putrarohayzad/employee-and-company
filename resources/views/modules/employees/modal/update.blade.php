<!-- Modal -->
@foreach ($employees as $employee)
<div class="modal fade" id="update-{{$employee->id}}" tabindex="-1" role="dialog" aria-labelledby="updatemodal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="updatemodal">Update Employee Information</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form id="updateEmployee-{{$employee->id}}" onsubmit="updateEmployee(event, {{$employee->id}})">
                <div class="modal-body">
                    @csrf
                    @method('PUT')
                    <div class="row">
                        <div class="col-md-6">
                            <div role="group" class="form-group">
                                <div class="form-row">
                                    <label class="form-label">First Name <span class="text-danger label-required">*</span></label>
                                    <input type="text" class="form-control" name="firstname" id="firstname-{{$employee->id}}" value="{{$employee->firstname}}" required>
                                </div>
                            </div>
                            <div role="group" class="form-group">
                                <div class="form-row">
                                    <label class="form-label">Company</label>x
                                    <select id="company-{{$employee->id}}" name="company" class="custom-select @error('company') is-invalid @enderror">
                                        <option value="SP" selected disabled>Please Select</option>
                                        @foreach  ($companies as $company)
                                            <option value="{{$company->id}}"
                                            @if ($company->id == old('company', $employee->company))
                                                selected
                                            @endif
                                            >{{$company->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div role="group" class="form-group">
                                <div class="form-row">
                                    <label class="form-label">Phone</label>
                                    <input type="number" class="form-control" id="phone-{{$employee->id}}" name="phone" value="{{$employee->phone}}">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div role="group" class="form-group">
                                <div class="form-row">
                                    <label class="form-label">Last Name <span class="text-danger label-required">*</span></label>
                                    <input type="text" class="form-control" id="lastname-{{$employee->id}}" name="lastname" value="{{$employee->lastname}}" required>
                                </div>
                            </div>
                            <div role="group" class="form-group">
                                <div class="form-row">
                                    <label class="form-label">Email</label> 
                                    <input type="email" class="form-control" id="email-{{$employee->id}}" name="email" value="{{$employee->email}}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button class="btn btn-success btn-submit"> <i class="fa fa-save"></i> Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endforeach

