<!-- Modal -->
@foreach ($companies as $company)
<div class="modal fade" id="update-{{$company->id}}" tabindex="-1" role="dialog" aria-labelledby="updatemodal"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="updatemodal">Update Company Information</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form onsubmit="updateCompany(event, {{$company->id}})" enctype="multipart/form-data">
                <div class="modal-body">
                    <div role="group" class="form-group text-center">
                        @if ($company->logo)
                        <img id="image-{{$company->id}}" alt="logo"
                            style="width: 100px; height:100px">
                        @else
                        <img src="{{asset('company.jpg')}}" alt="logo" width="250" height="250">
                        @endif
                        <div class="mt-3">
                            <button class="btn btn-primary" type="button">
                                <i class="fa fa-upload"></i> Upload
                                <input type="file" class="form-control mt-2" id="logo-{{$company->id}}" name="logo" accept="image/*">
                            </button>
                        </div>
                    </div>
                    <div role="group" class="form-group">
                        <div class="form-row">
                            <label class="form-label">Name<span class="text-danger label-required">*</span></label>
                            <input type="text" class="form-control" id="name-{{$company->id}}" name="name"  required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div role="group" class="form-group">
                                <div class="form-row">
                                    <label class="form-label">Email</label>
                                    <input type="email" class="form-control" id="email-{{$company->id}}" name="email" >
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div role="group" class="form-group">
                                <div class="form-row">
                                    <label class="form-label">Website</label>
                                    <input type="url" class="form-control" id="website-{{$company->id}}" name="website">
                                </div>
                            </div>
                        </div>
                    </div>
                 

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button class="btn btn-success"> <i class="fa fa-save"></i> Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endforeach