<!-- Modal -->
<div class="modal fade" id="create" tabindex="-1" role="dialog" aria-labelledby="createmodal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="createmodal">Create new company</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="createCompany" onsubmit="createCompany(event)" enctype="multipart/form-data">
                <div class="modal-body">
                    <div role="group" class="form-group text-center">
                        <img src="{{asset('company.jpg')}}" alt="logo" width="250" height="250">
                        <div class="mt-3">
                            <button class="btn btn-primary" type="button">
                                <i class="fa fa-upload"></i> Upload
                                <input type="file" class="form-control mt-2" name="logo" accept="image/*">
                            </button>
                        </div>
                    </div>
                    <div role="group" class="form-group">
                        <div class="form-row">
                            <label class="form-label">Name<span class="text-danger label-required">*</span></label>
                            <input type="text" class="form-control" name="name" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div role="group" class="form-group">
                                <div class="form-row">
                                    <label class="form-label">Email</label>
                                    <input type="email" class="form-control" name="email">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-row">
                                <label class="form-label">Website</label>
                                <input type="url" class="form-control" name="website">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button class="btn btn-success btn-submit"> <i class="fa fa-save"></i> Save</button>
                </div>
            </form>
        </div>
    </div>
</div>