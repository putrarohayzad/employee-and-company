@extends('layouts.app')

@section('content')

<div class="content p-3">
    <div class="card">
        <div class="card-body">
            <h3>List of Companies</h3>
            <br>
            <button type="button" class="btn btn-sm mb-3 text-white" style="background-color: #d39614 !important;"
                data-toggle="modal" data-target="#create">
                <i class="fa fa-plus"></i> &nbsp; | &nbsp; Create new company
            </button>
            <div class="table-responsive">
                <table class="table table-striped" id="companytable">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Logo</th>
                            <th scope="col">Name</th>
                            <th scope="col">Email</th>
                            <th scope="col">Website</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
            {{-- Create Modal --}}
            @include('modules.companies.modal.create')

            {{-- Update Modal --}}
            @include('modules.companies.modal.update')
        </div>
    </div>
</div>
@endsection
@section('js')
<script>
    $(document).ready(function(){
        $('#companytable').DataTable({
            processing:true,
            serverSide:true,
            ajax: {
                url: "{{route('companies.getCompanyData')}}",

            },
            columns: [
                {
                    name:'DT_RowIndex',
                    data:'DT_RowIndex',
                    orderable: false,
                    searchable: false
                },
                {
                    name:'logo',
                    data:'logo',
                    orderable: false,

                },

                {
                    name:'name',
                    data:'name'
                },
                {
                    name:'email',
                    data:'email'
                },
               
                {
                    name:'website',
                    data:'website',
                },
                {
                    name:'action',
                    data:'action',
                    orderable:false
                },

            ]
        });        
    });
</script>

<script>
     function createCompany (e) {
        e.preventDefault();

            const logo = document.querySelector('input[name=logo]');

            const formData = new FormData();
            formData.append('name', $('input[name=name]').val());
            formData.append('email', $('input[name=email]').val());
            formData.append('website', $('input[name=website]').val());
            formData.append('logo', logo.files[0]);

            const url = `/companies`;
             axios.post(url, formData)
            .then(res => {
                $('#createCompany')[0].reset();
                $("#create").modal('hide');
                iziToast.success({
                    title: 'OK',
                    message: res.data.msg,
                });
                $('#companytable').DataTable().ajax.reload();
            })
            .catch(e => {
                console.error(e);
                iziToast.error({
                    title: 'Error',
                    message: e.message,
                });
            });
           }

        async function editModal(company){
            try{
                const url = `/companies/findone/${company}`;
                const res = await axios(url);
                let srcData = `{{ URL::asset('/storage/${res.data.logo}')}}`;
                
                $(`#name-${company}`).val(res.data.name);
                $(`#email-${company}`).val(res.data.email);
                $(`#website-${company}`).val(res.data.website);
                $(`#image-${company}`).attr('src', srcData)
            }
            catch(e){
                console.error(e);
            }
        }

         function updateCompany (e, company) {
            e.preventDefault();

            const logo = document.querySelector(`#logo-${company}`);

            const formData = new FormData();
            formData.append('name', $(`#name-${company}`).val());
            formData.append('email', $(`#email-${company}`).val());
            formData.append('website', $(`#website-${company}`).val());
            formData.append('logo', logo.files[0]);

            const url = `/companies/${company}?_method=PUT`;
             axios.post(url, formData)
            .then(res => {
                $(`#update-${company}`).modal('hide');
                iziToast.success({
                    title: 'OK',
                    message: res.data.msg,
                });
                $('#companytable').DataTable().ajax.reload();
            })
            .catch(e => {
                  console.error(e);
                iziToast.error({
                    title: 'Error',
                    message: e.message,
                });
            });
        }

     function deleteCompany (company) {
            if(confirm("Are you sure?")){
                const url = `/companies/${company}`;
                 axios.delete(url)
                .then(res => {
                    iziToast.success({
                    title: 'OK',
                    message: res.data.msg,
                });
                    $('#companytable').DataTable().ajax.reload();
                })
                .catch(e => {
                      console.error(e);
                iziToast.error({
                    title: 'Error',
                    message: e.message,
                });
                });
            }
           }
</script>
@endsection