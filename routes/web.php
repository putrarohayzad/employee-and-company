<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
*/

Auth::routes(['register' => false]);

Route::get('/', function () {
    return view('welcome');
});


Route::group(['middleware' => ['auth', 'admin']], function () {

    Route::resource('companies', App\Http\Controllers\CompanyController::class)->except(['show']);    
    Route::resource('employees', App\Http\Controllers\EmployeeController::class)->except(['show']);
    
    //ajax
    Route::get('/companies/getcompanydata', [App\Http\Controllers\CompanyController::class, 'getCompanyData'])->name('companies.getCompanyData');
    Route::get('/companies/findone/{id}', [App\Http\Controllers\CompanyController::class, 'findOne'])->name('companies.findOne');
    Route::get('/employees/getemployeedata', [App\Http\Controllers\EmployeeController::class,'getEmployeeData'])->name('employees.getEmployeeData');
    Route::get('/employees/findcompany/{id}', [App\Http\Controllers\EmployeeController::class, 'findCompany'])->name('employees.findCompany');
});
