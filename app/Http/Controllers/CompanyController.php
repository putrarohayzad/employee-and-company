<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Repositories\CompanyRepositoryInterface;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    private $companyRepository;

    public function __construct(CompanyRepositoryInterface $companyRepository)
    {
        $this->companyRepository = $companyRepository;
    }

    public function index()
    {
        $companies = Company::all();
        return view('modules.companies.index', compact('companies'));
    }


    public function getCompanyData()
    {
        $companies = Company::select('companies.*');
        return $this->companyRepository->dataTable($companies);
    }

    public function findOne($id)
    {
        return $this->companyRepository->findOne($id);
    }


    public function store(Request $request)
    {
        $request->validate($this->validateData);

        $this->companyRepository->save($request);

        return response()->json([
            'msg' => 'Company have been created'
        ], 201);
    }

    public function update(Company $company, Request $request)
    {
        $request->validate($this->validateData);

        $this->companyRepository->update($company, $request);

        return response()->json([
            'msg' => 'Company have been updated'
        ], 201);
    }


    public function destroy(Company $company)
    {
        $company->delete();
        return response()->json([
            'msg' => 'Company have been deleted'
        ],202);
    }

    private $validateData = [
        'name' => 'required',
        'email' => 'nullable|email',
        'website' => 'nullable',
    ];
}
