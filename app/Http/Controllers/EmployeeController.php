<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Employee;
use App\Repositories\EmployeeRepositoryInterface;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    private $employeeRepository;

    public function __construct(EmployeeRepositoryInterface $employeeRepository)
    {
        $this->employeeRepository = $employeeRepository;
    }

    public function index()
    {
        $employees = Employee::all();
        $companies = Company::all();
        return view('modules.employees.index', compact('employees', 'companies'));
    }

    public function getEmployeeData()
    {
        $employees = Employee::select('employees.*');
        return $this->employeeRepository->dataTable($employees);
    }

    public function findCompany($id)
    {
        return $this->employeeRepository->findCompany($id);
    }


    public function store(Request $request)
    {
        $request->validate($this->validateData);

        $this->employeeRepository->save($request);
        return response()->json([
            'msg' => 'Employee  have been created'
        ], 201);
    }

    public function update(Employee $employee, Request $request)
    {
        $request->validate($this->validateData);

        $this->employeeRepository->update($employee, $request);
        return response()->json([
            'msg' => 'Employee  have been updated'
        ], 201);
    }

    public function destroy(Employee $employee)
    {
        $employee->delete();
        return response()->json([
            'msg' => 'Employee  have been deleted'
        ], 202);
    }

    private $validateData = [
        'firstname' => 'required',
        'lastname'  => 'required',
        'email'     => 'nullable|email',
    ];
}
