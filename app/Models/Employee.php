<?php

namespace App\Models;

use App\Models\Company;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{

    public function getCompany()
    {
        return $this->belongsTo(Company::class, 'company', 'id');
    }

 
    public function getFullNameAttribute()
    {
        return "{$this->firstname} {$this->lastname}";
    }
}


