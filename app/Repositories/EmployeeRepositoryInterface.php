<?php

namespace App\Repositories;


interface EmployeeRepositoryInterface
{

    public function save($request);
    public function update($employee, $request);
    public function dataTable($employees);
    public function findCompany($id);

}
