<?php

namespace App\Repositories;

use App\Models\Company;
use Yajra\DataTables\Facades\DataTables;
use App\Repositories\CompanyRepositoryInterface;


class CompanyRepository implements CompanyRepositoryInterface
{

    public function save($request)
    {
        $company = new Company();
        $company->name = $request->name;
        $company->email = $request->email;
        $company->website = $request->website;

        if ($request->file('logo')) {
            $logoFileName = $request->file('logo')->getClientOriginalName();
            $request->file('logo')->storeAs('public', $logoFileName);
            $company->logo = $logoFileName;
        }
        $company->save();
    }

    public function update($company , $request)
    {
        $company->name = $request->name;
        $company->email = $request->email;
        $company->website = $request->website;

        if ($request->file('logo')) {
            $logoFileName = $request->file('logo')->getClientOriginalName();
            $request->file('logo')->storeAs('public', $logoFileName);
            $company->logo = $logoFileName;
        }
        $company->save();

    }

    public function dataTable($companies)
    {
        return DataTables::eloquent($companies)
        ->addIndexColumn()
        ->addColumn('index', function ($company) {
            return $company->count();
        })
        ->addColumn('action', function ($company) {
            return '<button class="btn btn-sm btn-info text-white mb-3" data-toggle="modal" onclick="editModal( ' . $company->id . ')"
                    data-target="#update-' . $company->id . '">  
                    <i class="fa fa-edit"></i> &nbsp;  | &nbsp; Update
                    </button> 
                    &nbsp;'
                     .
                    '<button class="btn btn-sm btn-secondary mb-3 btn-submit" onclick="deleteCompany('.$company->id.')">
                        <i class="fa fa-trash"></i> &nbsp;  | &nbsp;  Delete
                    </button>
                    ';
        })
        ->editColumn('email', function ($company) {
            return $company->email ? $company->email : 'none'; 
            
        })
        ->editColumn('logo', function ($company) {
            if (!$company->logo) return '<img src="' . asset('company.jpg') . '" alt="logo" width="40" align="center" />';
            $url = asset('storage/' . $company->logo);
            return '<img src="' . $url . '" alt="logo" width="40" align="center" />';
        })
        ->editColumn('website', function ($company) {
            if (!$company->website) return '<a href="javascript:" class="btn btn-secondary btn-sm"> none </a>';
            return '<a href="' . $company->website . '" class="btn btn-primary btn-sm" target="_blank"> 
            <i class="fa fa-link"></i> &nbsp;  | &nbsp;' . $company->website . ' </a>';
        })
        ->rawColumns(['action', 'logo', 'website'])
        ->toJson();
    }

    public function findOne($id)
    {
        return Company::query()->where('id' , $id)->firstOrFail();

    }
}
