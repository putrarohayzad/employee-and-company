<?php

namespace App\Repositories;

use App\Models\Company;
use App\Models\Employee;
use Yajra\DataTables\Facades\DataTables;


class EmployeeRepository implements EmployeeRepositoryInterface
{

    public function save($request)
    {
        $employee            = new Employee;
        $employee->firstname = $request->input('firstname');
        $employee->lastname  = $request->input('lastname');
        $employee->company   = $request->input('company');
        $employee->email     = $request->input('email');
        $employee->phone     = $request->input('phone');

        $employee->save();
    }

    public function update($employee, $request)
    {
        $employee->firstname    = $request->input('firstname');
        $employee->lastname     = $request->input('lastname');
        $employee->company      = $request->input('company');
        $employee->email     = $request->input('email');
        $employee->phone        = $request->input('phone');

        $employee->save();
    }

    public function dataTable($employees)
    {
        return DataTables::eloquent($employees)
            ->addIndexColumn()
            ->addColumn('action', function ($employee) {
                return '<button class="btn btn-sm btn-info text-white mb-3" data-toggle="modal"
            data-target="#update-' . $employee->id . '">  
            <i class="fa fa-edit"></i> &nbsp;  | &nbsp; Update
            </button> 
            &nbsp;'
                    .
                    '<button class="btn btn-sm btn-secondary mb-3" onclick="deleteEmployee(' . $employee->id . ')">
                    <i class="fa fa-trash"></i> &nbsp;  | &nbsp;  Delete
            </button>
       ';
            })
            ->editColumn('company', function ($employee) {
                if (!$employee->getCompany) {
                    return '<a href="javascript:" class="btn btn-secondary btn-sm">none</a>';
                }
                return '<button class="btn btn-sm btn-primary text-white mb-3" data-toggle="modal" onclick="companyModal('. $employee->id . ','. $employee->getCompany->id . ')"
            data-target="#companyemployee-' . $employee->id . '">  
            <i class="fa fa-building"></i> &nbsp;  | &nbsp;' . $employee->getCompany->name . ' 
            </button> 
            ';
            })
            ->editColumn('email', function ($employee) {
                return $employee->email ? $employee->email : 'none';
            })
            ->editColumn('phone', function ($employee) {
                return $employee->phone ? $employee->phone : 'none';
            })

            ->addColumn('fullname', function ($employee) {
                return $employee->full_name;
            })
            ->rawColumns(['action', 'fullname', 'company', 'phone'])
            ->toJson();
    }

    
    public function findCompany($id)
    {
        return Company::query()->where('id' , $id)->firstOrFail();

    }
}
