<?php

namespace App\Repositories;

interface CompanyRepositoryInterface
{

    public function save($request);
    public function update($company , $request);
    public function dataTable($companies);
    public function findOne($id);
}