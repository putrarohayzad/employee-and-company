<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
            'name' => 'user',
            'email' => 'user@grtech.com.my',
            'password' => bcrypt('password'),
            ],
            [
            'name' => 'admin',
            'email' => 'admin@grtech.com.my',
            'password' => bcrypt('password'),
            ]
        ];

        DB::table('users')->insert($users);
    }
}
