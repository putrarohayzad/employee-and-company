<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $companies = [
            [
                'name' => 'Mario Tech Sdn Bhd',
                'email' => 'Mario@grtech.com.my',
                'logo' => null,
                'website' => 'http://127.0.0.1:8000/companies',
            ],
            [
                'name' => 'Luigi Consultant Sdn Bhd',
                'email' => 'Luigi@grtech.com.my',
                'logo' => null,
                'website' => 'http://127.0.0.1:8000/employees',
            ],
            [
                'name' => 'John Tech Sdn Bhd',
                'email' => 'John@grtech.com.my',
                'logo' => null,
                'website' => 'http://127.0.0.1:8000/companies',
            ],
            [
                'name' => 'Sonic Consultant Sdn Bhd',
                'email' => 'Sonic@grtech.com.my',
                'logo' => null,
                'website' => 'http://127.0.0.1:8000/employees',
            ],
        ];

        DB::table('companies')->insert($companies);
    }
}
