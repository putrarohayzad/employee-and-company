<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $employees = [
            [
                'firstname' => 'Mario',
                'lastname' => 'John',
                'company' => 1,
                'email' => 'mario@grtech.com.my',
                'phone' => '0123456789',
            ],
            [
                'firstname' => 'Luigi',
                'lastname' => 'Cassano',
                'company' => 2,
                'email' => 'Luigi@grtech.com.my',
                'phone' => '0143656789',
            ],
            [
                'firstname' => 'Lionel',
                'lastname' => 'Messi',
                'company' => 1,
                'email' => 'Messi@grtech.com.my',
                'phone' => '0133466789',
            ],
        ];

        DB::table('employees')->insert($employees);
    }
}
